package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	_ "github.com/jackc/pgx/stdlib"
)

var (
	updateQuery = `
		INSERT INTO users (name, date_of_birth) VALUES ($1, $2)
			ON CONFLICT (name) DO UPDATE
				SET date_of_birth = $2 WHERE users.name = $1`

	selectQuery = "SELECT name, to_char(Date_of_birth, 'YYYY-MM-DD') FROM users WHERE name = $1"
)

type User struct {
	Name        string `json:"name"`
	DateOfBirth string `json:"DateOfBirth"`
}

//TODO: return error
func (u *User) nextBirthday() (int, error) {
	today := today()
	birthday, err := parseDate(u.DateOfBirth)
	if err != nil {
		return 0, err
	}

	dayNow, monthNow := today.Day(), today.Month()
	dayBirth, monthBirth := birthday.Day(), birthday.Month()
	// The year of the next birthday
	yearBirth := today.Year()

	// Next birthday will be in next year
	if monthBirth < monthNow || (monthBirth == monthNow && dayBirth < dayNow) {
		yearBirth++
	}

	nextBirthday := date(yearBirth, monthBirth, dayBirth)

	return int(nextBirthday.Sub(today).Hours()) / 24, nil
}

type userHandler struct {
	db *sql.DB
}

func (u *userHandler) setUser(user *User) error {
	_, err := u.db.Exec(updateQuery, user.Name, user.DateOfBirth)
	return err
}

func (u *userHandler) getUser(name string) (*User, error) {
	var username string
	var DateOfBirth string
	err := u.db.QueryRow(selectQuery, name).Scan(&username, &DateOfBirth)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return &User{Name: username, DateOfBirth: DateOfBirth}, nil
}

func (u *userHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		name, err := extractUsername(r.URL)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		user, err := u.getUser(name)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if user == nil {
			http.Error(w, "User not found", http.StatusNotFound)
			return
		}

		days, err := user.nextBirthday()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if days > 0 {
			fmt.Fprintf(w, "Hello, %s! Your birthday is in %d day(s)!", user.Name, days)
		} else {
			fmt.Fprintf(w, "Hello, %s! Happy birthday!", user.Name)
		}
	} else if r.Method == "PUT" {
		user, err := parseRequest(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		err = u.setUser(user)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusNoContent)
	} else {
		http.Error(w, "Not implemented", http.StatusNotImplemented)
		return
	}
}

func extractUsername(url *url.URL) (string, error) {
	path := strings.SplitN(url.EscapedPath(), "/", 4)
	if len(path) < 3 {
		return "", fmt.Errorf("Wrong url '/hello/<username>': %s", url.EscapedPath())
	}

	name := path[2]
	if name == "" {
		return "", fmt.Errorf("Name should not be empty")
	}

	if !checkUsername(name) {
		return "", fmt.Errorf("username must contain only letters")
	}

	return name, nil
}

func parseRequest(r *http.Request) (*User, error) {
	var u User
	if r.Body == nil {
		return nil, fmt.Errorf("Request body is missing")
	}
	err := json.NewDecoder(r.Body).Decode(&u)
	if err != nil {
		return nil, fmt.Errorf("Cannot decode request body: %s", err.Error())
	}

	if _, err = parseDate(u.DateOfBirth); err != nil {
		return nil, fmt.Errorf("'dateOfBirth' has wrong format: %w", err)
	}

	u.Name, err = extractUsername(r.URL)
	if err != nil {
		return nil, err
	}

	return &u, nil
}
