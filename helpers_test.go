package main

import (
	"testing"
	"time"
)

func dateEqual(a time.Time, b time.Time) bool {
	if a.Day() != b.Day() {
		return false
	}
	if a.Month() != b.Month() {
		return false
	}
	if a.Year() != b.Year() {
		return false
	}

	return true
}

func TestDate(t *testing.T) {
	now := time.Now()
	date := date(now.Year(), now.Month(), now.Day())
	if !dateEqual(now, date) {
		t.Error("Dates are not equal")
	}

	if date.Nanosecond() != 0 {
		t.Error("nanoseconds must be 0")
	}
	if date.Second() != 0 {
		t.Error("seconds must be 0")
	}
	if date.Minute() != 0 {
		t.Error("minutes must be 0")
	}
	if date.Hour() != 0 {
		t.Error("hours must be 0")
	}
}

func TestToday(t *testing.T) {
	today := today()
	if today.Nanosecond() != 0 {
		t.Error("nanoseconds must be 0")
	}
	if today.Second() != 0 {
		t.Error("seconds must be 0")
	}
	if today.Minute() != 0 {
		t.Error("minutes must be 0")
	}
	if today.Hour() != 0 {
		t.Error("hours must be 0")
	}

	now := time.Now()
	if !dateEqual(now, today) {
		t.Error("Dates are not equal")
	}
}

func TestParseDate(t *testing.T) {
	got, err := parseDate("1986-05-17")
	if err != nil {
		t.Errorf("parseDate returns a error: %s", err)
	}
	expect := time.Date(1986, time.May, 17, 0, 0, 0, 0, time.UTC)
	if !dateEqual(got, expect) {
		t.Error("Dates are not equal")
	}

	got, err = parseDate("1986-00-28")
	if err == nil {
		t.Errorf("parseDate must return a error")
	}

	tomorrow := today().AddDate(0, 0, 1)
	got, err = parseDate(tomorrow.Format(dateFormat))
	if err == nil {
		t.Errorf("parseDate must return a error")
	}
}

func TestCheckUsername(t *testing.T) {
	if !checkUsername("vovka") {
		t.Errorf("name is valid, but checkUsername returns false")
	}
	if checkUsername("vovka667") {
		t.Errorf("name is invalid, but checkUsername returns true")
	}
}
