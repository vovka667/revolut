resource "kubernetes_service" "revolut" {
  metadata {
    name = "revolut"
  }

  spec {
    selector = {
      app = "revolut"
    }

    port {
      name        = "http"
      port        = 80
      target_port = "revolut"
    }

    type = "LoadBalancer"
  }

  depends_on = [
    kubernetes_deployment.revolut,
  ]
}

resource "kubernetes_deployment" "revolut" {
  metadata {
    name = "revolut"

    labels = {
      app = "revolut"
    }
  }

  spec {
    selector {
      match_labels = {
        app = "revolut"
      }
    }

    replicas = var.replicas
    template {
      metadata {
        labels = {
          app = "revolut"
        }
      }

      spec {
        container {
          image = "gcr.io/${var.project_id}/revolut:${var.environment}"
          name  = "revolut"

          image_pull_policy = "Always"

          liveness_probe {
            http_get {
              path = "/live"
              port = "revolut"
            }

            initial_delay_seconds = 3
            period_seconds        = 20
            timeout_seconds       = 5
          }

          readiness_probe {
            http_get {
              path = "/ready"
              port = "revolut"
            }

            initial_delay_seconds = 3
            period_seconds        = 20
            timeout_seconds       = 5
          }

          env {
            name = "PGHOST"

            value_from {
              secret_key_ref {
                name = "revolut-db"
                key  = "hostname"
              }
            }
          }

          env {
            name = "PGUSER"

            value_from {
              secret_key_ref {
                name = "revolut-db"
                key  = "username"
              }
            }
          }

          env {
            name = "PGDATABASE"

            value_from {
              secret_key_ref {
                name = "revolut-db"
                key  = "dbname"
              }
            }
          }

          env {
            name = "PGPASSWORD"

            value_from {
              secret_key_ref {
                name = "revolut-db"
                key  = "password"
              }
            }
          }

          port {
            name           = "revolut"
            protocol       = "TCP"
            container_port = "8080"
          }

          resources {
            limits {
              cpu    = "0.5"
              memory = "256Mi"
            }
            requests {
              cpu    = "250m"
              memory = "20Mi"
            }
          }
        }

        image_pull_secrets {
          name = "regcred"
        }
      }
    }
  }

  depends_on = [
    kubernetes_secret.revolut-db,
    google_container_cluster.revolut,
  ]
}

resource "kubernetes_secret" "revolut-db" {
  metadata {
    name = "revolut-db"
  }

  data = {
    dbname   = google_sql_database.revolut.name
    hostname = google_sql_database_instance.revolut.private_ip_address
    password = google_sql_user.revolut.password
    username = var.db_name
  }
}

output "app_url" {
  value = "http://${kubernetes_service.revolut.load_balancer_ingress[0].ip}/"
}
