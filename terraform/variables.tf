variable "project_id" {}
variable "environment" {
  default = "prod"
}
variable "region" {
  default = "us-central1"
}
variable "db_username" {
  default = "revolut"
}
variable "db_name" {
  default = "revolut"
}
variable "tier" {
  default = "db-f1-micro"
}
variable "replicas" {
  default = 2
}
