resource "google_compute_network" "private_network" {
  name = "private-network"
}

resource "google_compute_global_address" "private_ip_address" {
  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.private_network.self_link
}

resource "google_service_networking_connection" "private_vpc_connection" {
  network                 = google_compute_network.private_network.self_link
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}

resource "random_id" "db_name_suffix" {
  byte_length = 4
}

resource "google_sql_database_instance" "revolut" {
  name             = "revolut-${random_id.db_name_suffix.hex}"
  database_version = "POSTGRES_11"

  depends_on = [google_service_networking_connection.private_vpc_connection]

  settings {
    tier              = var.tier
    availability_type = "REGIONAL"
    ip_configuration {
      ipv4_enabled    = false
      private_network = google_compute_network.private_network.self_link
    }
  }
}

resource "google_sql_database" "revolut" {
  name     = var.db_name
  instance = google_sql_database_instance.revolut.name
}

resource "random_password" "password" {
  length  = 16
  special = false
}

resource "google_sql_user" "revolut" {
  name     = var.db_username
  instance = google_sql_database_instance.revolut.name
  //Do not use in real project. Password will be in state file.
  password = random_password.password.result
}
