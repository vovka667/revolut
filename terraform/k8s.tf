resource "google_container_cluster" "revolut" {
  name               = "revolut"
  location           = var.region
  initial_node_count = 1

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",
    ]

    metadata = {
      disable-legacy-endpoints = "true"
    }
  }

  ip_allocation_policy {
  }

  network = google_compute_network.private_network.self_link

  provisioner "local-exec" {
    command = "gcloud container clusters get-credentials revolut --region ${var.region} --project ${var.project_id}"
  }
}

provider "kubernetes" {}
