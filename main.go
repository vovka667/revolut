package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"

	migrate "github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/gorilla/handlers"
)

func getConnection() string {
	host := os.Getenv("PGHOST")
	if host == "" {
		host = "localhost"
	}
	dbname := os.Getenv("PGDATABASE")
	if dbname == "" {
		dbname = "revolut"
	}
	user := os.Getenv("PGUSER")
	password := os.Getenv("PGPASSWORD")

	return fmt.Sprintf("postgres://%s:%s@%s:5432/%s", user, password, host, dbname)
}

func migrateDB(dsn string) error {
	m, err := migrate.New("file://db/migrations", dsn)
	if err != nil {
		return err
	}
	err = m.Up()
	if err != migrate.ErrNoChange {
		return err
	} else {
		log.Println(err)
	}

	return nil
}

func main() {
	dsn := getConnection()
	err := migrateDB(dsn)
	if err != nil {
		log.Fatal(err)
	}

	db, err := sql.Open("pgx", dsn)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	h := &userHandler{
		db: db,
	}

	http.Handle("/hello/", handlers.LoggingHandler(os.Stdout, h))
	http.HandleFunc("/live", live)
	http.HandleFunc("/ready", handleReadiness(db))
	log.Fatal(http.ListenAndServe(":8080", nil))
}
