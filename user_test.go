package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
)

func TestUserDBMethods(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	user := &User{
		Name:        "vovka",
		DateOfBirth: "1986-05-17",
	}

	errUser := &User{
		Name:        "error",
		DateOfBirth: "error",
	}

	row := sqlmock.NewRows([]string{"name", "date_of_birth"}).
		AddRow(user.Name, user.DateOfBirth)
	noRow := sqlmock.NewRows([]string{"name", "date_of_birth"})
	errorRow := sqlmock.NewRows([]string{"name", "date_of_birth"}).
		AddRow(user.Name, user.DateOfBirth).
		RowError(0, fmt.Errorf("row error"))

	mock.ExpectExec("INSERT INTO users").WithArgs(user.Name, user.DateOfBirth)
	mock.ExpectExec("INSERT INTO users").WithArgs(errUser.Name, errUser.DateOfBirth).WillReturnError(fmt.Errorf("some error"))

	mock.ExpectQuery("SELECT").WithArgs(user.Name).WillReturnRows(row)
	mock.ExpectQuery("SELECT").WithArgs("wrong_username").WillReturnRows(noRow)
	mock.ExpectQuery("SELECT").WithArgs("error").WillReturnRows(errorRow)

	userHandler := &userHandler{db: db}
	userHandler.setUser(user)
	err = userHandler.setUser(errUser)
	if err == nil {
		t.Errorf("'error' username must return error")
	}

	userHandler.getUser(user.Name)
	_, err = userHandler.getUser("wrong_username")
	if err != nil {
		t.Errorf("Wrong username must return nil: %s", err)
	}
	_, err = userHandler.getUser("error")
	if err == nil {
		t.Errorf("'error' username must return error")
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestNextBirthday(t *testing.T) {
	week := today().AddDate(-10, 0, 7)
	u := &User{
		Name:        "vovka",
		DateOfBirth: week.Format(dateFormat),
	}
	next, err := u.nextBirthday()
	if err != nil {
		t.Errorf("nextBirthday() return error: %s", err)
	}
	if next != 7 {
		t.Error("nextBirthday() must return 7 if birthday is in one week")
	}

	yesterday := today().AddDate(0, 0, -1)
	u = &User{
		Name:        "vovka",
		DateOfBirth: yesterday.Format(dateFormat),
	}
	next, err = u.nextBirthday()
	if err != nil {
		t.Errorf("nextBirthday() return error: %s", err)
	}
	if next != 364 && next != 365 {
		t.Error("nextBirthday() must return 364 or 365 if birthday is yesterday")
	}

	today := today().AddDate(-10, 0, 0)
	u = &User{
		Name:        "vovka",
		DateOfBirth: today.Format(dateFormat),
	}
	next, err = u.nextBirthday()
	if err != nil {
		t.Errorf("nextBirthday() return error: %s", err)
	}
	if next != 0 {
		t.Error("nextBirthday() must return 0 if birthday is today")
	}

	incorrectDate := "1777-14-14"
	u = &User{
		Name:        "vovka",
		DateOfBirth: incorrectDate,
	}
	_, err = u.nextBirthday()
	if err == nil {
		t.Errorf("nextBirthday() must return an error")
	}
}

func TestServeHTTPGET(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	handler := &userHandler{db: db}

	type test struct {
		status int
		date   string
		row    *sqlmock.Rows
	}

	tests := map[string]test{
		"vovka667": {status: http.StatusBadRequest},
		"vovka": {
			status: http.StatusOK,
			date:   today().Format(dateFormat),
			row: sqlmock.NewRows([]string{"name", "date_of_birth"}).
				AddRow("vovka", today().AddDate(-10, 0, 0).Format(dateFormat)),
		},
		"notexists": {
			status: http.StatusNotFound,
			date:   today().Format(dateFormat),
			row:    sqlmock.NewRows([]string{"name", "date_of_birth"}),
		},
		"error": {
			status: http.StatusInternalServerError,
			date:   today().Format(dateFormat),
			row: sqlmock.NewRows([]string{"name", "date_of_birth"}).
				AddRow("vovka", today().Format(dateFormat)).
				RowError(0, fmt.Errorf("row error")),
		},
	}

	for name, test := range tests {
		if test.row != nil {
			mock.ExpectQuery("SELECT").WithArgs(name).WillReturnRows(test.row)
		}

		r := httptest.NewRequest("GET", "http://localhost/hello/"+name, nil)
		w := httptest.NewRecorder()
		handler.ServeHTTP(w, r)
		resp := w.Result()

		if resp.StatusCode != test.status {
			//t.Errorf("Wrong status code on wrong method. Got: %d, expected: %d", resp.StatusCode, test.status)
			body, _ := ioutil.ReadAll(resp.Body)
			t.Errorf(string(body))
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestServeHTTPNotImplemented(t *testing.T) {
	r := httptest.NewRequest("POST", "http://localhost/hello/vovka", nil)
	w := httptest.NewRecorder()

	new(userHandler).ServeHTTP(w, r)

	resp := w.Result()

	if resp.StatusCode != http.StatusNotImplemented {
		t.Errorf("Wrong status code on wrong method. Got: %d, expected: %d", resp.StatusCode, http.StatusNotImplemented)
	}
}

func TestServeHTTPPUT(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	handler := &userHandler{db: db}

	type test struct {
		status int
		name   string
		date   string
		query  bool
		err    error
	}

	tests := []test{
		{
			status: http.StatusNoContent,
			name:   "vovka",
			date:   "1986-05-17",
			query:  true,
			err:    nil,
		},
		{
			status: http.StatusBadRequest,
			name:   "vovka667",
			date:   "1986-05-17",
			query:  false,
			err:    nil,
		},
		{
			status: http.StatusInternalServerError,
			name:   "error",
			date:   "1986-05-17",
			query:  true,
			err:    fmt.Errorf("some error"),
		},
	}

	for _, test := range tests {
		if test.query {
			expect := mock.ExpectExec("INSERT INTO users").
				WithArgs(test.name, test.date).
				WillReturnResult(sqlmock.NewResult(1, 1))
			if test.err != nil {
				expect.WillReturnError(test.err)
			}
		}

		body := strings.NewReader(`{"dateOfBirth":"` + test.date + `"}`)
		r := httptest.NewRequest("PUT", "http://localhost/hello/"+test.name, body)
		w := httptest.NewRecorder()
		handler.ServeHTTP(w, r)
		resp := w.Result()

		if resp.StatusCode != test.status {
			t.Errorf("Wrong status code on wrong method. Got: %d, expected: %d", resp.StatusCode, test.status)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestExtractUsername(t *testing.T) {
	tests := map[string]string{
		"/hello/vovka":                            "vovka",
		"/hello/vovka/something/else":             "vovka",
		"/hello/vovka/something/else?with=params": "vovka",
	}

	for path, username := range tests {
		url, err := url.Parse(path)
		if err != nil {
			t.Errorf("Womething went wrong in url.Parse: %s", err)
		}

		parsedUsername, err := extractUsername(url)
		if err != nil {
			t.Errorf("extractUsername returns a error: %s", err)
		}
		if parsedUsername != username {
			t.Errorf("extractUsername returns wrong value")
		}
	}
}

func TestExtractUsernameError(t *testing.T) {
	tests := []string{
		"/hellovovka",
		"hellovovka",
		"hellovovka/",
		"/hello//vovka/",
		"/hello/vovka667/",
	}

	for _, path := range tests {
		url, err := url.Parse(path)
		if err != nil {
			t.Errorf("Womething went wrong in url.Parse: %s", err)
		}

		parsedUsername, err := extractUsername(url)
		if err == nil {
			t.Errorf("extractUsername must return an error for url: %s", path)
		}
		if parsedUsername != "" {
			t.Errorf("extractUsername returns not empty result on error: %s", err)
		}
	}
}

func TestParseRequest(t *testing.T) {
	type test struct {
		name string
		body string
		err  error
	}

	tests := []test{
		{
			name: "vovka",
			body: `{"dateOfBirth":"1986-05-17"}`,
		},
		{
			name: "brokenJSON",
			body: `{"dateOfBirth:"1986-05-17"}`,
			err:  fmt.Errorf("some error"),
		},
		{
			name: "incorrectDate",
			body: `{"dateOfBirth":"1986-00-17"}`,
			err:  fmt.Errorf("some error"),
		},
		{
			name: "vovka667",
			body: `{"dateOfBirth":"1986-05-17"}`,
			err:  fmt.Errorf("some error"),
		},
	}

	for _, test := range tests {
		body := strings.NewReader(test.body)
		r := httptest.NewRequest("GET", "http://localhost/hello/"+test.name, body)

		_, err := parseRequest(r)
		if err != nil && test.err == nil {
			t.Errorf("parseRequest returns error: %s", err)
		} else if err == nil && test.err != nil {
			t.Errorf("parseRequest doesn't return error")
		}
	}
}
