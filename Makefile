ifndef PROJECT
$(error PROJECT is not set)
endif

ENV ?= prod
APP=gcr.io/${PROJECT}/revolut:${ENV}

init:
	gcloud config set project ${PROJECT}

test:
	go test .

build: clean
	go build -o revolut

run:
	go run main.go user.go helpers.go handlers.go

clean:
	go clean

docker-build:
	docker build -t ${APP} .

docker-release: docker-build
	gcloud auth configure-docker
	docker push ${APP}

infra:
	cd terraform; terraform apply

update:
	kubectl rollout restart deployment/revolut
