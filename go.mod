module gitlab.com/Guck_Mal_GmbH/revolut

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.4.0
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/golang-migrate/migrate/v4 v4.8.0
	github.com/gorilla/handlers v1.4.2
	github.com/jackc/pgx v3.6.1+incompatible
	github.com/lib/pq v1.3.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/shopspring/decimal v0.0.0-20200105231215-408a2507e114 // indirect
	golang.org/x/crypto v0.0.0-20200115085410-6d4e4cb37c7d // indirect
)
