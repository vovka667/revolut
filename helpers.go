package main

import (
	"fmt"
	"regexp"
	"time"
)

const (
	dateFormat = "2006-01-02"
)

var (
	usernameRegexp = regexp.MustCompile(`^[[:alpha:]]+$`)
)

//Returns midnight time for specified date
func date(year int, month time.Month, day int) time.Time {
	return time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
}

func today() time.Time {
	now := time.Now()
	return date(now.Year(), now.Month(), now.Day())
}

func parseDate(d string) (time.Time, error) {
	t, err := time.Parse(dateFormat, d)
	if err != nil {
		return t, err
	}

	if !t.Before(today()) {
		return t, fmt.Errorf("Birthday should be in past")
	}

	return t, err
}

func checkUsername(name string) bool {
	return usernameRegexp.MatchString(name)
}
