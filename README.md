# 10,000 foot view

![10,000 foot view](/revolut.png)

# Get started

This guide assumes what you have a [GCP project](https://cloud.google.com/community/tutorials/managing-gcp-projects-with-terraform) and configured gcloud cli.
Docker and kubectl required as well.

## Required environment variables

```
export PROJECT=<project_id>
export export TF_VAR_project_id=${PROJECT}
```
Where project_id is a project id :). (Value from PROJECT_ID column in `gcloud projects list` command).

## Create a service account key

See [link](https://cloud.google.com/community/tutorials/getting-started-on-gcp-with-terraform) for details.
*Save your json file as terraform/CREDENTIALS_FILE.json*

## Build an image
```bash
make docker-release
```

## Create resources in the cloud
```bash
make infra
...
Apply complete! Resources: 3 added, 0 changed, 0 destroyed.

Outputs:

app_url = http://35.193.228.99/
```
Follow the link above.

# Update running application

## Build an image
```bash
make docker-release
```

## Run update
```bash
make update
```

# Update infrastructure
```bash
make infra
```
